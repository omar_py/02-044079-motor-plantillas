const express = require("express");

const app = express();

app.use(express.static("public"));

app.listen(3000, () => {
	console.log("El servidor se está ejecutando en http://localhost:3000");
});
